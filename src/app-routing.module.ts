import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './app/dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './app/register/register.component';
import { RunModelComponent } from './app/runmodel/runmodel.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: 'login' , pathMatch: 'full' },
  { path: 'register', component: RegisterComponent },
  { path: 'runmodel', component: RunModelComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
